package org.example;
import org.example.service.CashbackService;

public class Main {
  public static void main(String[] args) {
    CashbackService service = new CashbackService();
    int transaction = service.cashbackCalculate(100);
    System.out.println(transaction);
  }
}
